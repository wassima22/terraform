resource "aws_instance" "serveurWEL" {
        ami = "ami-017c629ce8712b5d4"
        instance_type = "t2.micro"
        key_name = "kodemade-aws"
        vpc_security_group_ids = ["${aws_security_group.Serveur-GS-WEL.id}"]
        associate_public_ip_address = true

tags = {
        Name = "Serveur-WEL"
}
}
resource "aws_security_group" "Serveur-GS-WEL" {
  name = "SSH-acces-wsm"
  ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
        } 
  egress {
	from_port = 0
	to_port = 0
	protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
	
	}
}

